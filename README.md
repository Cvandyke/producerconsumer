Author: Chandler Van Dyke
Course: CST-315
Assignment: Producer and Consumer
Professor: Glenn Williamson / Luke Kanuchok
Date: 1/28/2018

Approach:
My approach was to create main two threads, a producer and a consumer.
Along with the threads came the following functions for each producer and consumer:
    Producer
        produce
        put
    Consumer
        getItem
        consume
The technique i implemented in order to prevent the consumer from waiting 
and the producer not making too much was a delay for each.
For the producer, as the buffer filled up, the greater the delay.
For the consumer, as the buffer filled up, the smaller the delay.
Thus the producer would just slow down if the buffer got too big and the 
consumer would slow down as the buffer got emptier. 

