/*
Author: Chandler Van Dyke
Course: CST-315
Assignment: Producer and Consumer
Professor: Glenn Williamson / Luke Kanuchok
Date: 1/28/2018

Approach:
My approach was to create main two threads, a producer and a consumer.
Along with the threads came the following functions for each producer and consumer:
    Producer
        produce
        put
    Consumer
        getItem
        consume
The technique i implemented in order to prevent the consumer from waiting
and the producer not making too much was a delay for each.
For the producer, as the buffer filled up, the greater the delay.
For the consumer, as the buffer filled up, the smaller the delay.
Thus the producer would just slow down if the buffer got too big and the
consumer would slow down as the buffer got emptier.
*/

#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>

/*
Producer: main, produce, put
Consumer: main,  get, consume
*/

//INIT//
const int arrMax = 400;
const int arrEmpty = 0;
const int maxWait = 400000;

int isOpen = 1;

int currentIndex = 0;
int buffer[100];
int product = 0;


void delay(int milliseconds){
    clock_t currentTime = clock();

    while(clock() < currentTime + milliseconds);
}

//PRODUCER//
//produce
int produce(){
    return product++;
}
//put
void put(int item){
    buffer[currentIndex++]=item;
}
//main
void *producer(void *var){
    int producerWait;
    int currentProduct;
    printf("The Store is now Open\n");
    clock_t timeClose = clock() + 60000000; // Store is open for 1 minute
    while(clock() < timeClose){
        //wait amount
        producerWait = (currentIndex * 10000);
        delay(producerWait);

        //create product
        currentProduct = produce();
        //add prduct to buffer
        put(currentProduct);
    }
    //when store is closed, change sign
    isOpen = 0;
    printf("The Store is Now Closed\n");
}


//CONSUMER//
//get
int getItem(){
    return buffer[--currentIndex];
}
//consume
void consume(int itemID){
    printf("Consuming item %i at currentIndex %i \n", itemID, currentIndex);
}
//main
void *consumer(void *var){
    int consumerWait;
    int currentConsumption;
    //while the store is open
    while(isOpen){
        //wait amount
        consumerWait = maxWait - (currentIndex *10000);
        delay(consumerWait);

        //get item from store
        currentConsumption = getItem();
        //consume item
        consume(currentConsumption);
    }
}

int main() {
    pthread_t prodthread;
    pthread_t conthread;
    int proCheck;
    int conCheck;

    //Initialize threads
    proCheck = pthread_create(&prodthread, NULL, producer, NULL);
    //check to see if producer thread created correctly
    if(proCheck){
        printf("ERROR when creating producer thread: %d", proCheck);
        exit(-1);
    }    
 
    conCheck = pthread_create(&conthread, NULL, consumer, NULL);
    //check to see if consumer thread created correctly
    if(conCheck){
        printf("ERROR when creating consumer thread: %d", conCheck);
        exit(-1);
    }    
    pthread_exit(NULL);
}

